package models;

public class Client {
	 private String login;
		private String pwd;
		private String email;
		private String nomPrenom;

	    public Client(String login, String pwd, String email, String nomPrenom) {
	        this.login = login;
	        this.pwd = pwd;
	        this.email = email;
	        this.nomPrenom = nomPrenom;
	    }

	    public Client() {
	    }
	        
	        
		public String getLogin() {
			return login;
		}
		public void setLogin(String login) {
			this.login = login;
		}
		public String getPwd() {
			return pwd;
		}
		public void setPwd(String pwd) {
			this.pwd = pwd;
		}
		public String getEmail() {
			return email;
		}
		public void setEmail(String email) {
			this.email = email;
		}
		public String getNomPrenom() {
			return nomPrenom;
		}
		public void setNomPrenom(String nomPrenom) {
			this.nomPrenom = nomPrenom;
		}
		
	   
}
